package com.guosen;

import com.guosen.zebra.ZebraRun;
import com.guosen.zebra.core.grpc.anotation.ZebraConf;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ZebraConf(confName="zebra.gateway")
@ServletComponentScan
public class App {
    public static void main(String[] args) throws Exception {
        ZebraRun.run(args, App.class, true);
    }
}
